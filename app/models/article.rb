class Article < ActiveRecord::Base
  attr_accessible :title, :text, :images, :users_id

  validates :title, length: { minimum: 5 }, presence: true
  validates :text, length: {minimum: 20}, presence: true

  mount_uploader :images, ImagesUploader
  validate :image_size_validation

  belongs_to :user
  has_many :ratings
  has_many :comments

  def image_size_validation
    ::Rails.logger.info "Image should be less than 2MB"
    errors.add :images, "Image should be less than 2MB" if images.size > 2.megabytes
  end

  def average_rating
     ratings.average(:score)
  end

end

