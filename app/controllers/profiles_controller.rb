class ProfilesController < ApplicationController
    def create
      @user = current_user
      @profile = @user.build_profile(profile_params)
      @profile.save

      redirect_to root_path
    end

    def update
      @user = current_user
      @profile = @user.profile.update_attributes(profile_params)

      redirect_to root_path

    end

    def profile_params
      params.require(:profile).permit(:phone, :mobile_phone, :address, :city, :country, :blog, :avatar, :extras)
    end

end
