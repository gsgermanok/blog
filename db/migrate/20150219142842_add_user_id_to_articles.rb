class AddUserIdToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :users, index: true
  end

  def self.down
    remove_column :articles, :users
  end
end
