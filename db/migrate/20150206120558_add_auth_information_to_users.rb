class AddAuthInformationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :uid, :integer # uid of omniauth service
    add_column :users, :provider, :string # name of service for example twitter, git hub
    add_column :users, :views, :integer # how much times the visitor income to the page
  end

  def self.down

    remove_column :users, :uid
    remove_column :users, :service
  end

end
